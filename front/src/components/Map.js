import React from 'react'
import { Map, Marker, GoogleApiWrapper } from 'google-maps-react'
import styled from 'styled-components'

export class MapContainer extends React.Component {
    fetchPlaces(mapProps, map) {
        // const { google } = mapProps
        // const service = new google.maps.places.PlacesService(map)

        console.log('Loaded')
    }

    onMarkerClick = () => {}

    render() {
        const MapFrame = styled.div`
            position: absolute;
            width: 100%;
            height: 100%;
        `

        return (
            <div>
                <MapFrame>
                    <Map
                        google={this.props.google}
                        zoom={17}
                        // 62.0396826,129.7406279,17z
                        initialCenter={{ lat: 62.0396826, lng: 129.7406279 }}
                        onReady={this.fetchPlaces}
                        mapTypeControl={false}
                        disableDefaultUI={true}
                    >
                        {/* <Marker
                            onClick={this.onMarkerClick}
                            name={'Current location'}
                            draggable={true}
                            id={0}
                        /> */}
                        <Marker
                            name={'Your position'}
                            position={{ lat: 62.0396826, lng: 129.7396279 }}
                            icon={{
                                url: 'https://www.dropbox.com/s/xxgumzmwhj8kokd/parkingIcon.png?dl=1',
                                anchor: new this.props.google.maps.Point(32, 32),
                                scaledSize: new this.props.google.maps.Size(40, 40)
                            }}
                        />
                        <Marker
                            name={'Two point'}
                            position={{ lat: 62.0407026, lng: 129.7416379 }}
                            icon={{
                                url: 'https://www.dropbox.com/s/yjiyt23ecmrcsg8/2point.png?dl=1',
                                anchor: new this.props.google.maps.Point(32, 32),
                                scaledSize: new this.props.google.maps.Size(32, 40)
                            }}
                        />
                        <Marker
                            name={'Two point#2'}
                            position={{ lat: 62.0393026, lng: 129.7418379 }}
                            icon={{
                                url: 'https://www.dropbox.com/s/yjiyt23ecmrcsg8/2point.png?dl=1',
                                anchor: new this.props.google.maps.Point(32, 32),
                                scaledSize: new this.props.google.maps.Size(32, 40)
                            }}
                        />
                    </Map>
                </MapFrame>
            </div>
        )
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyAeGLHbhy-BhtopOW1j6dj44iaIFVEab4M'
})(MapContainer)
