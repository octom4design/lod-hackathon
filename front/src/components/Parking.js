import React from 'react'
import styled from 'styled-components'
import parkingIcon from './../parkingIcon.png'

export default () => {
    const Frame = styled.div`
        position: absolute;
        background-color: #f2f2f2;
        display: flex;
        flex-direction: column;
        height: 100vh;
        width: 100%;
        align-items: center;
        justify-content: flex-start;
    `

    const TopBarContainer = styled.div`
        /* flex:1; */
        margin-top: 20px;
        margin-bottom: 20px;
        /* background-color: red; */
        /* padding: 25px 25px 25px 25px; */
    `
    const TopBar = styled.div`
        /* background-color: green; */
        display: flex;
        flex-direction: row;
        /* width: 50vh; */
    `
    const TopBarSearch = styled.div`
        flex: 6;
        width: 35vh;
        /* background-color: red; */
    `
    const Input = styled.input.attrs((props) => ({
        // we can define static props
        type: 'text',

        // or we can define dynamic ones
        size: props.size || '0.65em'
    }))`
        width: 80%;
        color: black;
        /* font-size: 1em; */
        border: 0px solid palevioletred;
        border-radius: 3px;

        /* here we use the dynamically computed prop */
        margin: ${(props) => props.size};
        padding: ${(props) => props.size};
    `

    const TopBarAdd = styled.div`
        text-align: center;
        text-justify: center;
        color: white;
        font-size: 25px;
        font-weight: 700;
        width: 1.6em;
        height: 1.6em;
        background-color: #0dd954;
        border-radius: 5px;
        box-shadow: 0 0 2px 2px rgba(0, 0, 0, 0.25);
    `
    return (
        <Frame>
            <a href="/map">{"<"}Back</a>
            <TopBarContainer>
                <TopBar>
                    <div
                        style={{
                            flex: 1,
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <TopBarAdd>+</TopBarAdd>
                    </div>
                    <TopBarSearch>
                        <Input placeholder="Hint text"></Input>
                    </TopBarSearch>
                </TopBar>
                <Points city={"г.Якутск"} title={"ул. Петровского, 14"}/>
                <Points city={"г.Якутск"} title={"ул. Орджоникидзе, 25"}/>
            </TopBarContainer>
        </Frame>
    )
}

const Points = (props) => {
    const Frame = styled.div`
        padding-top: 15px;
        height: 4em;
        display: flex;
        flex-direction: row;
        align-items: center;
        /* background-color: gray; */
    `

    const TextFrame = styled.div`
        height: 100%;
        width: 100%;
        /* background-color: blue; */
        display: flex;
        flex-direction: column;
        /* align-items: flex-start; */
    `
    const TextTitle = styled.div`
        flex:4;
        justify-self: center;
        font-weight: 700;
        font-size: 20px;
        margin: 5px;
        margin-top: 10px;
        margin-left: 10px;
        color: #A1A1A1;
        /* background-color: red; */
    `
    return (
        <Frame>
            <img alt src={parkingIcon} style={{ width: '50px', height: '50px' }} />
            <TextFrame>
    <TextTitle>{props.title} </TextTitle>
                <div style={{flex: 1, alignSelf: "flex-end", color: '#79A3F7'}}>{props.city}</div>
            </TextFrame>
        </Frame>
    )
}
