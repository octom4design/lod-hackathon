import React from 'react'
import { Link } from 'react-router-dom'
import { bool } from 'prop-types'
import { StyledMenu } from './Menu.styled'
const Menu = ({ open }) => {
    return (
        <StyledMenu open={open}>
            <Link to="/profile">
                {/* <span role="img" aria-label="about us">&#x1f481;&#x1f3fb;&#x200d;&#x2642;&#xfe0f;</span> */}
                Профиль
            </Link>
            <Link to="/myParks">
                {/* <span role="img" aria-label="price">&#x1f4b8;</span> */}
                Добавить мои парковки
            </Link>
            <Link to="/">
                {/* <span role="img" aria-label="contact">&#x1f4e9;</span> */}
                Настройки
            </Link>
        </StyledMenu>
    )
}
Menu.propTypes = {
    open: bool.isRequired
}
export default Menu
