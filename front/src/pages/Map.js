import React, { useState } from 'react'
import Map from './../components/Map'
import styled from 'styled-components'
import { Burger, Menu } from './../components'
import { ThemeProvider } from 'styled-components'
import { GlobalStyles } from './../options/global'
import { theme } from './../options/theme'
import parkingIcon from './../2point.png'
import { Link } from 'react-router-dom'

export default () => {
    const [open, setOpen] = useState(false)
    const [near, setNear] = useState(false)

    const UIFrame = styled.div`
        /* position: absolute; */
    `
    const BottomMenu = styled.div`
        position: absolute;
        bottom: 0;
        left: 10px;
        right: 10px;
        /* height: 100%; */
        background: #f2f2f2;
        border-radius: 10px 10px 0 0;
    `

    const Card = styled.div`
        display: flex;
    `
    const Hola = styled.div`
        flex: 1;
        /* display: flex; */
        height: 100%;
        padding: 10px;
        justify-content: center;
        align-items: center;
        color: white;
    `
    const CardA = styled.div`
        display: flex;
        flex-direction: column;
        align-items: center;
    `
    return (
        <>
            <Map />
            <BottomMenu>
                {near ? (
                    <CardA>
                        <button onClick={()=>{setNear(!near)}}>{"<"}Back</button>
                        <Points title={'ул. Петровского, 14'} point={"Свободно 2 места"}/>
                        <Points title={'Орджоникидзе, 25'} point={"Свободно 2 места"}/>
                    </CardA>
                ) : (
                    <Card>
                        <Hola>
                            <div
                                style={{
                                    display: 'flex',
                                    height: '15vh',
                                    width: '100%',
                                    // padding: '10px',
                                    background: '#0DD954',
                                    // margin: '10px',
                                    borderRadius: '15px',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    color: 'white'
                                }}
                            >
                                <button
                                    onClick={() => {
                                        setNear(!near)
                                    }}
                                    style={{
                                        background: "rgba(0,0,0,0)",
                                        borderColor: "rgba(0,0,0,0)",
                                        color: "white",
                                        height: "100%",
                                        width: "100%"
                                    }}
                                >
                                    <h5 style={{ textAlign: 'center', textJustify: 'center' }}>
                                        Мои парковки
                                    </h5>
                                </button>
                            </div>
                        </Hola>
                        <Hola>
                            <div
                                style={{
                                    display: 'flex',
                                    height: '15vh',
                                    width: '100%',
                                    // margin: '10px',
                                    borderRadius: '15px',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    background: '#1658EB'
                                }}
                            >
                                <button
                                    onClick={() => {
                                        setNear(!near)
                                    }}
                                    style={{
                                        background: "rgba(0,0,0,0)",
                                        borderColor: "rgba(0,0,0,0)",
                                        color: "white",
                                        height: "100%",
                                        width: "100%"
                                    }}
                                >
                                <h5>Рядом</h5>
                                </button>
                            </div>
                        </Hola>
                    </Card>
                )}
            </BottomMenu>
            <UIFrame>
                <ThemeProvider theme={theme}>
                    <>
                        <GlobalStyles />
                        <div>{/* <Map /> */}</div>
                        <div>
                            <Burger open={open} setOpen={setOpen} />
                            <Menu open={open} setOpen={setOpen} />
                        </div>
                    </>
                </ThemeProvider>
            </UIFrame>
        </>
    )
}

const Points = (props) => {
    const Frame = styled.div`
        padding-top: 15px;
        padding-bottom: 15px;
        height: 4em;
        display: flex;
        flex-direction: row;
        align-items: center;
        /* background-color: gray; */
    `

    const TextFrame = styled.div`
        height: 100%;
        width: 100%;
        /* background-color: blue; */
        display: flex;
        flex-direction: column;
        align-items: center;
        /* align-items: flex-start; */
    `
    const TextTitle = styled.div`
        flex: 1;
        justify-self: center;
        font-weight: 700;
        font-size: 16px;
        /* margin: 5px; */
        /* margin-top: 10px; */
        margin-left: 10px;
        color: black;
        /* background-color: red; */
    `
    const Line = styled.div`
        height: 2px;
        width: 75%;
        background-color: #cecece;
    `
    return (
        <>
            <Frame>
                <img alt src={parkingIcon} style={{ width: '30px', height: '35px' }} />
                <TextFrame>
                    <TextTitle>{props.title} </TextTitle>
                    <div style={{ flex: 1, color: "black", paddingLeft: "10px", alignSelf: "flex-start"}}>
                        {props.point}
                    </div>
                </TextFrame>
            </Frame>
            <Line/>
        </>
    )
}
