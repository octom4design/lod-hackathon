import React from 'react';

import logo from './logo.svg';
import './App.css';

import Navigator from './navigation'

function App() {
  return (
    <Navigator/>
  );
}

export default App;
