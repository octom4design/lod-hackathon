import React from 'react'
import { ReactComponent as Logo } from './../myparking.svg'
import { ReactComponent as People } from './../People.svg'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import styled from 'styled-components'
import PinInput from 'react-pin-input'
import splashImage from './splash.png'
import { MapPage } from './../pages/index.ts'
import { Parking } from './../components'

export default () => {
    return (
        <Router>
            <div>
                <Switch>
                    <Route path="/map">
                        <MapPage />
                    </Route>
                    <Route path="/profile">
                        <Login />
                    </Route>
                    <Route path="/profile-pin">
                        <Pin />
                    </Route>
                    <Route path="/myParks">
                        <Parking/>
                    </Route>
                    <Route path="/">
                        <SplashScreen />
                    </Route>
                </Switch>
            </div>
        </Router>
    )
}

const Login = () => {
    const Home = styled.div`
        background-color: #f2f2f2;
        display: flex;
        flex-direction: column;
        height: 100vh;
        width: 100%;
        align-items: center;
        justify-content: center;
    `
    const Button = styled.button`
        /* Adapt the colors based on primary prop */
        background: ${(props) => (props.primary ? 'red' : 'white')};
        color: ${(props) => (props.primary ? 'white' : 'red')};

        font-size: 1em;
        margin: 2em;
        padding: 0.5em 2em;
        border: 2px solid palevioletred;
        border-radius: 10px;
        
        box-shadow: 0px 2px 4px 2px rgba(0,0,0,0.25);
    `
    const Input = styled.input.attrs((props) => ({
        // we can define static props
        type: 'text',

        // or we can define dynamic ones
        size: props.size || '1em'
    }))`
        color: black;
        font-size: 1em;
        border: 0px solid palevioletred;
        border-radius: 3px;

        /* here we use the dynamically computed prop */
        margin: ${(props) => props.size};
        padding: ${(props) => props.size};
    `
    return (
        <Home>
            <People style={{ margin: 50 }} />
            <Input placeholder="Введите номер" />
            <Link to={'/profile-pin'}>
                <Button primary>Войти</Button>
            </Link>
        </Home>
    )
}

const Pin = () => {
    const Home = styled.div`
        background-color: #f2f2f2;
        display: flex;
        flex-direction: column;
        height: 100vh;
        width: 100%;
        align-items: center;
        justify-content: center;
    `
    const Button = styled.button`
        /* Adapt the colors based on primary prop */
        background: ${(props) => (props.primary ? '#1658EB' : 'white')};
        color: ${(props) => (props.primary ? 'white' : '#1658EB')};

        font-size: 1em;
        margin: 2em;
        padding: 0.5em 2em;
        border: 0px solid;
        border-radius: 10px;
        box-shadow: 0px 2px 4px 2px rgba(0,0,0,0.25);
    `
    return (
        <Home>
            <h4>Введите код отправленный на Ваш номер</h4>
            <PinInput
                length={5}
                focus
                // disabled
                // secret
                // ref={(p) => (this.pin = p)}
                type="numeric"
                // onChange={this.onChange}
            />
            <Link to={'/map'}>
                <Button primary>Войти</Button>
            </Link>
        </Home>
    )
}

const SplashScreen = () => {
    const Splash = styled.div`
        /* background-image:  */
        background: url(${splashImage}), linear-gradient(183.55deg, #0339d9 35.17%, #1d64f2 91.28%); 
        background-size: 100%;
        background-repeat: no-repeat;
    `

    return (
        <Splash
            style={{
                width: '100%',
                height: '100vh',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
            }}
        >
            <Link to={'/profile'}>
                <Logo />
            </Link>
        </Splash>
    )
}

